import React from "react";
import Review from "./Component/Review";

function App() {
  return (
    <main>
      <Review />
    </main>
  );
}

export default App;
