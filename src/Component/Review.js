import React, { useState, useEffect } from "react";
import { FaChevronLeft, FaChevronRight, FaQuoteRight } from "react-icons/fa";

function Review() {
  const [index, setIndex] = useState(0);
  const [user, setUser] = useState([]);
  const [loading, setLoading] = useState(true);

  const checkNumber = (number) => {
    if (number > user.length - 1) {
      return 0;
    }
    if (number < 0) {
      return user.length - 1;
    }
    return number;
  };

  const randomPerson = () => {
    let rendomNumber = Math.floor(Math.random() * user.length);
    if (rendomNumber === index) {
      rendomNumber = index + 1;
    }
    setIndex(checkNumber(rendomNumber));
  };

  const back = () => {
    setIndex((index) => {
      let newIndex = index - 1;
      return checkNumber(newIndex);
    });
  };

  const forword = () => {
    setIndex((index) => {
      let newIndex = index + 1;
      return checkNumber(newIndex);
    });
  };

  useEffect(() => {
    fetch("https://reqres.in/api/users")
      .then((resp) => resp.json())
      .then((data) => {
        setUser(data.data);
        setLoading(false);
      })
      .catch((error) => console.log(error));
  }, []);

  if (loading) {
    return <h1>Loading...</h1>;
  }

  const { avatar, first_name, last_name, email } = user[index];

  return (
    <article className="review">
      <div className="img-container">
        <img src={avatar} alt={first_name} className="person-img" />
        <span className="quote-icon">
          <FaQuoteRight />
        </span>
      </div>
      <h4 className="author">
        {first_name} {last_name}
      </h4>
      <p className="job">{email}</p>
      {/* <p className="info">{text}</p> */}
      <div className="button-container">
        <button className="prev-btn" disabled>
          <FaChevronLeft onClick={back} />
        </button>
        <button className="next-btn">
          <FaChevronRight onClick={forword} />
        </button>
      </div>
      <button className="random-btn" onClick={randomPerson}>
        Random
      </button>
    </article>
  );
}

export default Review;
